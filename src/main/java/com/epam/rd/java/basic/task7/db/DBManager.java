package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.*;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;

	private static final String SELECT_ALL_USERS = "SELECT * FROM users";
	private static final String SELECT_ALL_TEAMS = "SELECT * FROM teams";
	private static final String SELECT_USER_BY_LOGIN ="SELECT * FROM users WHERE login=?";
	private static final String SELECT_TEAM_BY_NAME ="SELECT * FROM teams WHERE name=?";
	private static final String SELECT_TEAM_BY_USER ="SELECT * FROM teams WHERE id in " +
													 "(select team_id from users_teams where user_id in " +
													 "(select id from users where id=?))";

	private static final String INSERT_USER = "INSERT INTO users values (DEFAULT,?)";
	private static final String INSERT_TEAM = "INSERT INTO teams values (DEFAULT,?)";
	private static final String INSERT_TEAMS_FOR_USERS = "INSERT INTO users_teams values (?,?)";

	private static final String DELETE_TEAM = "delete from teams  where id=?";
	private static final String DELETE_USER = "delete from users where id=?";

	private static final String UPDATE_TEAM = "update teams set name = ? where id = ?";




	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {

	}

	public Connection getConnection() throws SQLException{
		 String URL = null;
		try(InputStream input = new FileInputStream("app.properties")) {
			Properties properties = new Properties();
			properties.load(input);
			URL = properties.getProperty("connection.url");
		} catch (IOException e) {
			e.printStackTrace();
		}
		if(URL != null){
			return DriverManager.getConnection(URL);
		}
		throw new SQLException();
	}

	private void close(AutoCloseable ac){
		if(ac!=null){
			try {
				ac.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public List<User> findAllUsers() throws DBException {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		List<User> users = new ArrayList<>();
		try{
			conn = instance.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(SELECT_ALL_USERS);
			while (rs.next()){
				User user = new User();
				user.setId(rs.getInt(1));
				user.setLogin(rs.getString(2));
				users.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(conn);
			close(stmt);
			close(rs);
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			conn = instance.getConnection();
			stmt = conn.prepareStatement(INSERT_USER,Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, user.getLogin());
			int c = stmt.executeUpdate();
			if(c == 0)return false;
			rs = stmt.getGeneratedKeys();
			if(rs.next()){
				user.setId(rs.getInt(1));
			}
		}catch (SQLException e){
			throw new DBException("Something went wrong",e);
		}finally {
			close(conn);
			close(stmt);
			close(rs);
		}
		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {
		Connection connection = null;
		PreparedStatement ps = null;
		int count = 0;
		try {
			connection = instance.getConnection();
			for (User user : users){
				ps = connection.prepareStatement(DELETE_USER);
				ps.setInt(1,user.getId());
				count+=ps.executeUpdate();
			}
			if(count>0)return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(connection);
			close(ps);
		}

		return false;
	}

	public User getUser(String login) throws DBException {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		User user = null;
		try{
			conn = instance.getConnection();
			stmt = conn.prepareStatement(SELECT_USER_BY_LOGIN);
			stmt.setString(1,login);
			rs = stmt.executeQuery();
			while (rs.next()){
				user = new User();
				user.setId(rs.getInt(1));
				user.setLogin(rs.getString(2));
			}
		} catch (SQLException e) {
			throw new DBException("Something went wrong",e);
		}finally {
			close(conn);
			close(stmt);
			close(rs);
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Team team = null;
		try{
			conn = instance.getConnection();
			stmt = conn.prepareStatement(SELECT_TEAM_BY_NAME);
			stmt.setString(1,name);
			rs = stmt.executeQuery();
			while (rs.next()){
				team = new Team();
				team.setId(rs.getInt(1));
				team.setName(rs.getString(2));
			}
		} catch (SQLException e) {
			throw new DBException("Something went wrong",e);
		}finally {
			close(conn);
			close(stmt);
			close(rs);
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		List<Team> teams = new ArrayList<>();
		try{
			conn = instance.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(SELECT_ALL_TEAMS);
			while (rs.next()){
				Team team = new Team();
				team.setId(rs.getInt(1));
				team.setName(rs.getString(2));
				teams.add(team);
			}
		} catch (SQLException e) {
			throw new DBException("Something went wrong",e);
		}finally {
			close(conn);
			close(stmt);
			close(rs);
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			conn = instance.getConnection();
			stmt = conn.prepareStatement(INSERT_TEAM,Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, team.getName());
			int c = stmt.executeUpdate();
			if(c == 0)return false;
			rs = stmt.getGeneratedKeys();
			if(rs.next()){
				team.setId(rs.getInt(1));
			}
		}catch (SQLException e){
			throw new DBException("Something went wrong with insertion",e);
		}finally {
			close(conn);
			close(stmt);
			close(rs);
		}
		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection connection = null;
		PreparedStatement ps = null;
		if(user == null || Arrays.stream(teams)
								 .anyMatch(Objects::isNull))
			throw new IllegalArgumentException();

		try{
			connection = instance.getConnection();
			connection.setAutoCommit(false);


			for (Team team: teams) {
				ps = connection.prepareStatement(INSERT_TEAMS_FOR_USERS);
				ps.setInt(1,user.getId());
				ps.setInt(2,team.getId());
				ps.executeUpdate();
			}
			connection.commit();
			connection.setAutoCommit(true);
			return true;
		} catch (SQLException e) {
			rollback(connection);
			throw new DBException("Something went wrong",e);
		}finally {
			close(connection);
			close(ps);
		}
	}

	private void rollback(Connection conn){
		try {
			conn.rollback();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<Team> teams = new ArrayList<>();
		try{
			conn = instance.getConnection();
			stmt = conn.prepareStatement(SELECT_TEAM_BY_USER);
			stmt.setInt(1,user.getId());
			rs = stmt.executeQuery();
			while (rs.next()){
				Team team = new Team();
				team.setId(rs.getInt(1));
				team.setName(rs.getString(2));
				teams.add(team);
			}
		} catch (SQLException e) {
			throw new DBException("Something went wrong",e.getCause());
		} finally {
			close(conn);
			close(stmt);
			close(rs);
		}
		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = instance.getConnection();
			ps = connection.prepareStatement(DELETE_TEAM);
			ps.setInt(1,team.getId());
			if(ps.executeUpdate()>0) return true;
		}catch (SQLException e) {
			throw new DBException("Something went wrong",e.getCause());
		}finally {
			close(connection);
			close(ps);
		}
		return false;
	}

	public boolean updateTeam(Team team) throws DBException {
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = instance.getConnection();
			ps = connection.prepareStatement(UPDATE_TEAM);
			ps.setString(1,team.getName());
			ps.setInt(2,team.getId());
			if(ps.executeUpdate()>0)return true;
		} catch (SQLException e) {
			throw new DBException("Something went wrong",e.getCause());
		}finally {
			close(connection);
			close(ps);
		}
		return false;
	}

}
